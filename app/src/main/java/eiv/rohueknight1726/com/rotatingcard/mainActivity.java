package eiv.rohueknight1726.com.rotatingcard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.TextView;

public class mainActivity extends AppCompatActivity {

    CardView outerCard;
    TextView countText;
    int count = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        outerCard = (CardView)findViewById(R.id.outerCard);
        countText = (TextView)findViewById(R.id.countText);
        countText.setText(""+count);

    }

    public void revealCard(View v){

        final View myView = findViewById(R.id.outerFrame);
        myView.animate().translationX(-200).alpha(0).setDuration(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                myView.setVisibility(View.VISIBLE);
                myView.animate().translationX(0).alpha(1).setDuration(200).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animateCard();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
//        int cx = myView.getWidth()/2;
//        int cy = myView.getHeight()/2;
//        float finalRadius = (float) Math.hypot(cx, cy);
//        Animator anim =
//                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
//        myView.setVisibility(View.VISIBLE);
//        anim.setDuration(300);
//        anim.start();
//        anim.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationCancel(Animator animation) {
//                super.onAnimationCancel(animation);
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation){
//
//                    animateCard();
//
//            }
//        });
    }

    private void animateCard(){
        outerCard.animate().setDuration(1000).rotationBy(360).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(count==1){
                    count--;
                    countText.setText(""+count);
                    reverseReveal();
                }
                else{
                    count--;
                    animateCard();
                    countText.setText(""+count);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();

    }

    private void reverseReveal(){
        final View myView = findViewById(R.id.outerFrame);
        myView.animate().translationX(-200).alpha(0).setDuration(200).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                myView.setVisibility(View.INVISIBLE);
                count = 10;
                countText.setText(""+count);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
//        int cx = myView.getWidth()/2;
//        int cy = myView.getHeight()/2;
//        float finalRadius = (float) Math.hypot(cx, cy);
//        Animator anim =
//                ViewAnimationUtils.createCircularReveal(myView, cx, cy, finalRadius, 0);
//        anim.setDuration(300);
//        anim.start();
//        anim.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationCancel(Animator animation) {
//                super.onAnimationCancel(animation);
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation){
//                myView.setVisibility(View.INVISIBLE);
//            }
//        });
    }
}
